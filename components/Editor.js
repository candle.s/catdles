import React, { Component } from 'react'

import {
  ImageSideButton,
  Block,
  addNewBlock,
  createEditorState,
  Editor
} from 'medium-draft'
import mediumDraftExporter from 'medium-draft/lib/exporter';

const blockButtons = [{
  label: 'H1',
  style: 'header-one',
  icon: 'header',
  description: 'Heading 1',
}, {
  label: 'Q',
  style: 'blockquote',
  icon: 'quote-right',
  description: 'Blockquote',
}, {
  label: 'UL',
  style: 'unordered-list-item',
  icon: 'list-ul',
  description: 'Unordered List',
}];

const inlineButtons = [{
  label: 'B',
  style: 'BOLD',
  icon: 'bold',
  description: 'Bold',
}, {
  label: 'I',
  style: 'ITALIC',
  icon: 'italic',
  description: 'Italic',
}, {
  label: 'U',
  style: 'UNDERLINE',
  icon: 'underline',
  description: 'Underline',
}, {
  label: 'S',
  style: 'STRIKETHROUGH',
  icon: 'strikethrough',
  description: 'Strikethrough',
}, {
  label: '#',
  style: 'hyperlink',
  icon: 'link',
  description: 'Add a link',
}]

class CustomImageSideButton extends ImageSideButton {
  onChange(e) {
    const file = e.target.files[0];
    if (file.type.indexOf('image/') === 0) {
      const formData = new FormData();
      formData.append('image', file);
      fetch('/post/image_post', {
        method: 'POST',
        body: formData,
      }).then((response) => {
        if (response.status === 200) {
          return response.json().then(data => {
            if (data.url) {
              this.props.setEditorState(addNewBlock(
                this.props.getEditorState(),
                Block.IMAGE, {
                  src: data.url,
                }
              ));
            }
          });
        }
      });
    }
    this.props.close();
  }
}

const sideButtons = [{
  title: 'Image',
  component: CustomImageSideButton,
}]

class EditorComponent extends Component {
  render() {
    const { editorState } = this.props
    return (
      <div>
        <Editor
          ref={this.refsEditor}
          editorState={editorState}
          onChange={this.props.onChange}
          inlineButtons={inlineButtons}
          blockButtons={blockButtons}
          sideButtons={sideButtons} />
      </div>
    )
  }
}

export default EditorComponent