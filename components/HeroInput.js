import React, { Component } from 'react'
import Styled from 'styled-components'
import { Formik, Form, Field, ErrorMessage } from 'formik';

class HeroInput extends Component {
  state = {
    responseEmail: null
  }

  render() {
    return (
      <Section>
        <Formik
          initialValues={{ email: '' }}
          validate={values => {
            let errors = {};
            if (!values.email) {
              errors.email = 'Required';
            } else if (
              !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
            ) {
              errors.email = 'Invalid email address';
            }
            return errors;
          }}
          onSubmit={async (values, { setSubmitting }) => {
            setSubmitting(false)
            this.setState({
              responseEmail: 'Loading...'
            })
            const res = await fetch('/sendmail', {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
              },
              body: JSON.stringify(values)
            })
            const response = await res.json()
            this.setState({
              responseEmail: response.msg
            })
          }}
        >
          {({ isSubmitting }) => (
            <Form>
              <Field type="email" name="email" />
              <button type="submit" disabled={isSubmitting}>Subscribe</button>
              <ErrorMessage name="email" component="div" style={{ color: 'red' }} />
              <br />{this.state.responseEmail}
            </Form>
          )}
        </Formik>
      </Section>
    )
  }
}

export default HeroInput

const Section = Styled.section`
  padding-top: 40px;
  
  input {
    font-size: 24px;
    width: 60%;
    height: 40px;
    border-radius: 4px;
    border: none;
    background-color: #eaeaea;
    padding: 0 15px;
  }

  button {
    font-size: 24px;
    margin-left: 10px;
    width: 130px;
    height: 40px;
    background-color: ${props => props.theme.secondaryColor};
    border-radius: 4px;
    border: none;
    color: #fff;
    transition: all 0.2s;
    &:active {
      background-color: ${props => props.theme.primaryColor};
    }
  }

  @media (max-width: 414px) {
    width: 100%;

    form {
      display: flex;
      align-items: center;
    }
    
    button {
      width: 30%;
      font-size: 16px;
    }
  }
`