import React, { Component } from 'react'
import Styled from 'styled-components'

import HeroInput from './HeroInput'

export default class HeroSec extends Component {
  render() {
    return (
      <Section>
        <Detail>
          <Header>
            <h1>Do you love cat?</h1>
            <img src="/static/images/cat-foot.png" alt="รอยเท้าแมว" />
          </Header>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates placeat voluptate optio modi, laboriosam ratione enim totam. In repellat commodi delectus accusamus quo. Obcaecati reiciendis debitis cum, dolores amet ratione!</p>
          <HeroInput />
        </Detail>
        <HeroImage src="/static/images/cat_home.svg" alt="แมว" />
      </Section>
    )
  }
}

const Section = Styled.section`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 40px 0;
  height: 100vh;

  @media (max-width: 414px) {
    padding: 0;
    height: 60vh;
  }
`

const Detail = Styled.div`
  flex: 0 65%;
  font-size: 20px;
  line-height: 1.6;

  h1 {
    font-size: 48px;
    color: black;
    text-transform: uppercase;
    padding-bottom: 20px;
  }
  
  @media (max-width: 414px) {
    flex: 1;
    padding: 0 40px;

    h1 {
      font-size: 26px;
    }
  }
`

const Header = Styled.div`
  display: flex;

  img {
    color: ${props => props.theme.primaryColor};
    padding-left: 10px;
    width: 70px;
    height: 60px;
  }

  @media (max-width: 414px) {
    img {
      display: none;
    }
  }
`

const HeroImage = Styled.img`
  height: 100%;

  @media (max-width: 414px) {
    display: none;
  }
`
