import React, { useState } from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik';
import Styled from 'styled-components'

const submitForm = async values => {
  const res = await fetch('/user/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    },
    body: JSON.stringify(values)
  })
  return await res.json()
}

const Login = (props) => {
  const [useMessage, setMessage] = useState('')
  return (
    <React.Fragment>
      <h1>Login</h1>
      <Formik
        initialValues={{ email: '', password: '' }}
        validate={values => {
          let errors = {};
          if (!values.email) {
            errors.email = 'Required';
          } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
          ) {
            errors.email = 'Invalid email address';
          } else if (!values.password) {
            errors.password = 'Required'
          }
          return errors;
        }}
        onSubmit={ async (values, { setSubmitting }) => {
          setSubmitting(false);
          const submit = await submitForm(values)
          setMessage('')
          if(submit.success) {
            window.location.href = '/'
          } else {
            setMessage(submit.msg)
          }
        }}
      >
        {({ isSubmitting }) => (
          <Form>
            <InputGroup>
              <label htmlFor="email">email: </label>
              <Field type="email" name="email" />
              <ErrorMessage name="email" component="div" className="error-message" />
            </InputGroup>
            <InputGroup>
              <label htmlFor="password">password: </label>
              <Field type="password" name="password" />
              <ErrorMessage name="password" component="div" className="error-message" />
            </InputGroup>
            <ErrorResponse>
            {useMessage}
            </ErrorResponse>
            <Button type="submit" disabled={isSubmitting}>
              Submit
            </Button>
          </Form>
        )}
      </Formik>
      <HandleForm>don't have an account ? <span onClick={props.handleState}>sign up</span></HandleForm>
    </React.Fragment>
  )
}

export default Login

const InputGroup = Styled.div`
  width: 100%;
  padding: 10px 0;
  
  input {
    width: 100%;
    height: 40px;
    font-size: 24px;
    padding: 0 10px;
    margin-top: 10px;
    border: none;
    background: #ddd;
    border-radius: 3px;
  }

  .error-message {
    color: red;
    font-size: 20px;
    padding: 10px 0;
  }
`

const Button = Styled.button`
  margin: 10px 0;
`

const HandleForm = Styled.p`
  font-size: 16px;
  padding: 10px 0;

  span {
    cursor: pointer;
    color: ${props => props.theme.primaryColor};
    font-weight: bold;
  }
`

const ErrorResponse = Styled.div`
  color: red;
  font-size: 20px;
`