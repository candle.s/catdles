import React, { Component } from 'react'
import Link from 'next/link'
import Styled from 'styled-components'
import jsCookie from 'js-cookie'

class Nav extends Component {
  state = {
    isOpen: false,
    menuTransform: false
  }

  scrollMenu = () => {
    const scrollY = window.pageYOffset || document.documentElement.scrollTop
    if(scrollY >= 90) {
      this.setState({ menuTransform: true })
    } else {
      this.setState({ menuTransform: false })
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.scrollMenu, false)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.scrollMenu, false)
  }

  toggleMenu = () => {
    this.setState(prevState => ({
      isOpen: !prevState.isOpen
    }))
  }

  renderAuthen = user => {
    let menu
    if (user) {
      menu = <Link href="/">
        <a onClick={() => jsCookie.remove('token')}>Logout</a>
      </Link>
    } else {
      menu = <Link href="/authen">
        <a>Login</a>
      </Link>
    }
    return menu
  }

  renderUser = user => {
    let profileImg
    if(user) {
      if(user.img) {
        profileImg = <Link href="/profile">
          <a><img src={`/static/user/${user.img}`} alt="user" /></a>
        </Link>
      } else {
        profileImg = <Link href="/profile">
          <a><img src="/static/images/user.svg" alt="user" /></a>
        </Link>
      }
    }
    return profileImg
  }

  render() {
    const { isOpen, menuTransform } = this.state
    const { user } = this.props
    return (
      <NavStyle transForm={menuTransform}>
        <WrapperNav>
          <Logo>
            <Link href="/">
              <a>Catdles</a>
            </Link>
          </Logo>
          <NavBerger onClick={this.toggleMenu} className={`${isOpen ? 'nav-open' : ''}`}>
            <div className="berger_line"></div>
            <div className="berger_line"></div>
            <div className="berger_line"></div>
          </NavBerger>
          <NavListGroup isOpen={isOpen}>
            <NavList>
              <Link href="/">
                <a>Home</a>
              </Link>
            </NavList>
            <NavList>
              <Link href="/cats">
                <a>Cats</a>
              </Link>
            </NavList>
            <NavList>
              <Link href="/blog">
                <a>Blog</a>
              </Link>
            </NavList>
            <NavList>
              {this.renderAuthen(user)}
            </NavList>
            <NavList>
              {this.renderUser(user)}
            </NavList>
          </NavListGroup>
        </WrapperNav>
      </NavStyle>
    )
  }
}

export default Nav

const NavStyle = Styled.div`
  height: 80px;
  margin: 0 auto;
  background-color: ${props => props.theme.primaryColor};
  position: ${props => props.transForm ? 'fixed' : 'initial'};
  left: ${props => props.transForm ? 0 : ''};
  width: ${props => props.transForm ? '100%' : ''};
  z-index: ${props => props.transForm ? 9 : ''};
  top: 0;

  @media (max-width: 414px) {
    width: 100%;
    height: auto;
    padding: 20px 0;
  }
`

const WrapperNav = Styled.nav`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 1180px;
  height: 100%;
  margin: 0 auto;
  font-size: 2rem;
  text-transform: uppercase;

  a {
    font-weight: bold;
    cursor: pointer;
    text-decoration: none;
    color: #fff;
  }

  @media (max-width: 414px) {
    width: 100%;
    height: auto;
    padding: 0 15px;
    flex-wrap: wrap;
  }
`

const Logo = Styled.div`
  flex: 0 55%;
  
  a {
    width: 20px;
  }

  @media (max-width: 414px) {
    flex: 0 85%;
  } 
`
const NavBerger = Styled.div`
  display: none;
  height: 30px;

  .berger_line {
    width: 100%;
    height: 5px;
    background: #fff;
  }

  @media (max-width: 414px) {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    flex: 1;

    .berger_line {
      width: 80%;
    }
  }
`

const NavListGroup = Styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex: 1;
  width: 100%;

  @media (max-width: 414px) {
    display: ${props => props.isOpen ? 'block' : 'none'};
    flex-direction: column;
    padding: 10px 0;
  }
`

const NavList = Styled.div`
  a {
    transition: 0.2s;
  }

  a:hover {
    color: ${props => props.theme.secondaryColor}
  }

  img {
    width: 40px;
    height: 40px;
    border-radius: 50%;
  }

  @media (max-width: 414px) {
    padding: 10px 0;
  }
`


