import React from 'react'
import Styled from 'styled-components'
import Nav from '../components/Nav'

export default function Auth(Component) {
  return class extends React.Component {

    render() {
      const { user } = this.props

      if(!user) {
        return (
          <div>
            <Nav />
            <Section>
              Please Login!
            </Section>
          </div>
        )
      }

      return (
        <Component {...this.props} />
      )
    }
  }
}

const Section = Styled.section`
  width: 1180px;
  height: auto;
  margin: 0 auto;
  font-size: 38px;
  padding: 40px 0;
  color: ${props => props.theme.secondaryColor}
`