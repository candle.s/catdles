import React from 'react'
import App, { Container } from 'next/app'
import Head from 'next/head'
import { ServerStyleSheet, createGlobalStyle } from 'styled-components';
import { ThemeProvider } from 'styled-components'
import NProgress from 'nprogress'
import Router from 'next/router'
import fetch from 'isomorphic-unfetch'
import jsCookie from 'js-cookie'

const theme = {
  primaryColor: 'rgb(219, 112, 147)',
  secondaryColor: 'rgb(252, 158, 8)',
}

Router.events.on('routeChangeStart', url => {
  NProgress.start()
})
Router.events.on('routeChangeComplete', () => {
  NProgress.done()
})
Router.events.on('routeChangeError', () => {
  NProgress.done()
})

const GlobalStyled = createGlobalStyle`
  *, 
  *::before, 
  *::after { 
    margin: 0;
    padding: 0;
    box-sizing: inherit;
  }
  html {
    box-sizing: border-box;
    font-size: 62.5%;
  }
  body {
    font-family: montserrat;
    font-weight: 400;
  }
  *:focus {
    outline: none;
  }
  a {
    text-decoration: none;
  }
`

class MyApp extends App {
  static async getInitialProps({ Component, router, ctx }) {
  
    let user = null

    const baseUrl = ctx.req ? `${ctx.req.protocol}://${ctx.req.get('Host')}` : '';

    if(process.browser) {
      const token = jsCookie.get('token')
      if (token) {
        const res = await fetch(`${baseUrl}/authentication`, {
          method: 'POST',
          headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
          }
        })
        user = await res.json()
      }
    } else {
      if(ctx.req.headers.cookie) {
        const token = ctx.req.headers.cookie.split('=')[1]
        if (token) {
          const res = await fetch(`${baseUrl}/authentication`, {
            method: 'POST',
            headers: {
              'Authorization': `Bearer ${token}`,
              'Content-Type': 'application/json'
            }
          })
          user = await res.json()
        }
      }
    }

    let pageProps = {}

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }

    return { pageProps, user }
  }

  componentDidMount() {
    if (typeof window.__REACT_DEVTOOLS_GLOBAL_HOOK__ === "object") {
      for (let [key, value] of Object.entries(window.__REACT_DEVTOOLS_GLOBAL_HOOK__)) {
        window.__REACT_DEVTOOLS_GLOBAL_HOOK__[key] = typeof value == "function" ? ()=>{} : null;
      }
    }
  }

  render() {
    const { Component, pageProps, user } = this.props

    return (
      <Container>
        <GlobalStyled />
        <Head>
          <meta charSet="utf-8" /> 
          <meta name="description" content="Catdles web for everyone" />
          <meta name="keywords" content="Cat,Catdles,Nextjs,react" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <link rel="icon" type="image/x-icon" href="../static/images/favicon.ico" />
          <link rel="stylesheet" href="/static/nprogress.css" />
          <link rel="stylesheet" type="text/css" href="https://unpkg.com/medium-draft/dist/medium-draft.css" />
          <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" />
          <title>catdles</title>
        </Head>
        <ThemeProvider theme={theme}>
          <Component {...pageProps} user={user} />
        </ThemeProvider>
      </Container>
    )
  }
}

export default MyApp