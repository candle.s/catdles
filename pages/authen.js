import React, { Component } from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik';
import Styled from 'styled-components'

import Nav from '../components/Nav'
import Login from '../components/Login'
import Register from '../components/Register'

class Authentication extends Component {
  state = {
    state: 'login'
  }

  handleState = () => {
    this.setState(prevState => ({
      state: prevState.state === 'login' ? 'register' : 'login'
    }))
  }

  render() {
    const { state } = this.state
    return (
      <div>
        <Nav user={this.props.user} />
        <Section>
          <SectionImage src={`${state === 'login' ? '/static/images/cat.svg' : '/static/images/cat_register.svg'}`} alt="cat" />
          <FormSection>
            {
              state === 'login' ? <Login handleState={this.handleState} /> : <Register handleState={this.handleState} />
            }
          </FormSection>
        </Section>
      </div>
    )
  }
}

export default Authentication

const Section = Styled.section`
  width: 1180px;
  height: 100vh;
  margin: 0 auto;
  display: flex;

  @media (max-width: 414px) {
    width: 100%;
    flex-direction: column;
    margin: 40px 0;
  }
`

const SectionImage = Styled.div`
  background-image: url('${props => props.src}');
  background-position: center;
  background-repeat: no-repeat;
  background-size: contain;
  flex: 0 50%;

  @media (max-width: 414px) {
    margin: 0 20px;
  }
`

const FormSection = Styled.div`
  flex: 1;
  margin: 120px 80px;

  h1 {
    text-transform: uppercase;
    font-size: 36px;
    color: ${props => props.theme.primaryColor};
    padding-bottom: 15px;
  }

  label {
    font-size: 20px;
  }

  button {
    width: 30%;
    height: 50px;
    font-size: 24px;
    color: #fff;
    background-color: ${props => props.theme.secondaryColor};
    border: none;
    border-radius: 3px;
  }

  @media (max-width: 414px) {
    margin: 0;
    padding: 40px 20px;

    button {
      width: 100%;
    }
  }
`