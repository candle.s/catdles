import React, { Component } from 'react'
import Link from 'next/link'
import Styled from 'styled-components'

import fetch from 'isomorphic-unfetch'

import Nav from '../components/Nav'

let url
const dev = process.env.NODE_ENV !== 'production'

class Blog extends Component {
  static async getInitialProps({req}) {
    const baseUrl = req ? `${req.protocol}://${req.get('Host')}` : ''
    const res = await fetch(`${baseUrl}/post/posts`)
    const posts = await res.json()

    return { posts }
  }

  renderPosts = () => {
    const { posts } = this.props
    if(posts.length < 1) {
      return <div style={{ 
        display: 'flex', 
        flex: '1', 
        fontSize: '24px', 
        justifyContent: 'center',
      }}>No result</div>
    } else {
      return posts.map(post => {
        return (
          <Link href={`/post?id=${post._id}`} key={post._id}>
            <a>
            <PostBox src={post.thumbnail}>
              <div>
                <span>{post.title}</span>
              </div>
            </PostBox>
            </a>
          </Link>
        )
      })
    }
  }

  render() {
    const { user } = this.props
    return (
      <div>
        <Nav user={this.props.user} />
        <Section>
          {
            user && (
              <NewArticles>
                {
                  <Link href="/newpost">
                    <a><button>New articles</button></a>
                  </Link>
                }
              </NewArticles>
            )
          }
          <SectionBlog>
            <RightSection>
              {this.renderPosts()}
            </RightSection>
          </SectionBlog>
        </Section>
      </div>
    )
  }
}

export default Blog

const Section = Styled.section`
  width: 1180px;
  height: 100vh;
  margin: 0 auto;

  @media (max-width: 414px) {
    width: 100%;
  }
`

const NewArticles = Styled.div`
  width: 100%;
  padding: 50px 50px 0;
  text-align: right;
  
  button {
    color: #fff;
    background-color: ${props => props.theme.primaryColor};
    border: none;
    width: 120px;
    height: 40px;
    text-transform: uppercase;
  }

  @media (max-width: 414px) {
    padding: 20px;
  }
`

const SectionBlog = Styled.section`
  display: flex;
  justify-content: space-between;
`

const RightSection = Styled.section`
  flex: 1;
  display: flex;
  flex-wrap: wrap;
  padding: 20px 0;

  a {
    flex: 0 32%;
  }


  @media (max-width: 414px) {
    flex-direction: column;
    flex-wrap: nowrap;
    padding: 20px;
  }
`

const PostBox = Styled.div`
  margin: 0 5px;
  display: flex;
  justify-content: center;
  align-items: flex-end;
  margin-top: 10px;
  background-image: url('/static/post/${props => props.src}');
  background-repeat: no-repeat;
  background-position: center;
  background-size: 100% 100%;
  height: 250px;

  div {
    opacity: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 40px;
    width: 100%;
    background-color: rgba(255, 0, 0, 0.4);
    color: #fff;
    font-size: 20px;
    font-weight: bold;
    transition: 0.4s;
  }

  &:hover {
    div {
      opacity: 1;
    }
  }
`