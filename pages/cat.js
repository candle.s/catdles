import React from 'react'
import Head from 'next/head'
import fetch from 'isomorphic-unfetch'
import Styled from 'styled-components'

import Nav from '../components/Nav'

const renderCat = (props) => {
  const { cat } = props
  return cat.map((item, index) => {
    if (item === undefined || item === null) return (<p>something wrong</p>)
    return (
      <React.Fragment>
        <Head>
          <title>{item.breeds[0].name}</title>
        </Head>
        <CatSection key={index}>
          <div className="cat_image">
            <CatImage src={item.url} alt={item.breeds[0].name} />
          </div>
          <div className="cat_detail">
            <CatDetail>
              <CatHeader>
                <h2>{item.breeds[0].name}</h2>
                <span>
                  <img
                    src={`https://lipis.github.io/flag-icon-css/flags/4x3/${item.breeds[0].country_code.toLowerCase()}.svg`}
                    alt={`cat ${item.breeds[0].country_code.toLowerCase()}.svg`}
                  />
                </span>
              </CatHeader>
              <CatDescription>{item.breeds[0].description}</CatDescription>
              <CatAbility>
                <label>adaptability</label>
                <div>
                  {loopStar(item.breeds[0].adaptability)}
                </div>
              </CatAbility>
              <CatAbility>
                <label>affection level</label>
                <div>
                  {loopStar(item.breeds[0].affection_level)}
                </div>
              </CatAbility>
              <CatAbility>
                <label>child friendly</label>
                <div>
                  {loopStar(item.breeds[0].child_friendly)}
                </div>
              </CatAbility>
              <CatAbility>
                <label>dog friendly</label>
                <div>
                  {loopStar(item.breeds[0].dog_friendly)}
                </div>
              </CatAbility>
              <CatAbility>
                <label>energy level</label>
                <div>
                  {loopStar(item.breeds[0].energy_level)}
                </div>
              </CatAbility>
              <CatAbility>
                <label>experimental</label>
                <div>
                  {loopStar(item.breeds[0].experimental)}
                </div>
              </CatAbility>
              <CatAbility>
                <label>grooming</label>
                <div>
                  {loopStar(item.breeds[0].grooming)}
                </div>
              </CatAbility>
              <CatAbility>
                <label>hairless</label>
                <div>
                  {loopStar(item.breeds[0].hairless)}
                </div>
              </CatAbility>
              <CatAbility>
                <label>health issues</label>
                <div>
                  {loopStar(item.breeds[0].health_issues)}
                </div>
              </CatAbility>
              <CatAbility>
                <label>hypoallergenic</label>
                <div>
                  {loopStar(item.breeds[0].hypoallergenic)}
                </div>
              </CatAbility>
              <CatAbility>
                <label>intelligence</label>
                <div>
                  {loopStar(item.breeds[0].intelligence)}
                </div>
              </CatAbility>
              <CatAbility>
                <label>natural</label>
                <div>
                  {loopStar(item.breeds[0].natural)}
                </div>
              </CatAbility>
              <CatAbility>
                <label>rare</label>
                <div>
                  {loopStar(item.breeds[0].rare)}
                </div>
              </CatAbility>
              <CatAbility>
                <label>shedding level</label>
                <div>
                  {loopStar(item.breeds[0].shedding_level)}
                </div>
              </CatAbility>
              <CatAbility>
                <label>short legs</label>
                <div>
                  {loopStar(item.breeds[0].short_legs)}
                </div>
              </CatAbility>
              <CatAbility>
                <label>social needs</label>
                <div>
                  {loopStar(item.breeds[0].social_needs)}
                </div>
              </CatAbility>
              <CatAbility>
                <label>stranger friendly</label>
                <div>
                  {loopStar(item.breeds[0].stranger_friendly)}
                </div>
              </CatAbility>
              <CatAbility>
                <label>suppressed tail</label>
                <div>
                  {loopStar(item.breeds[0].suppressed_tail)}
                </div>
              </CatAbility>
            </CatDetail>
          </div>
        </CatSection>
      </React.Fragment>
    )
  })
}

function loopStar(number) {
  const star = []
  for (let i = 1; i <= 5; i++) {
    let selectStar = '/static/images/star_sharp.svg'
    if (number >= i && number !== null) {
      selectStar = '/static/images/star.svg'
    }
    star.push(
      <AbilityStar src={selectStar} alt="star" />
    )
  }
  return star
}

const Cat = (props) => (
  <div>
    <Nav user={props.user} />
    <Section>
      {renderCat(props)}
    </Section>
  </div>
)

Cat.getInitialProps = async ({ query: { id } }) => {
  const res = await fetch(`https://api.thecatapi.com/v1/images/search?breed_ids=${id}`)
  const cat = await res.json()
  return { cat }
}

export default Cat

const Section = Styled.div`
  width: 1180px;
  height: auto;
  margin: 0 auto;

  @media (max-width: 414px) {
    width: 100%;
  }
`

const CatSection = Styled.div`
  width: 100%;
  height: auto;
  padding: 50px 0;
  display: flex;

  .cat_image {
    flex: 0 40%;
  }

  .cat_detail {
    flex: 1;
    margin-left: 50px;
  }

  @media (max-width: 414px) {
    flex-direction: column;

    .cat_image {
      padding: 20px;
    }

    .cat_detail {
      margin-left: 0;
    }
  }
`

const CatImage = Styled.div`
  width: 100%;
  height: 500px;
  background-image: url('${props => props.src}');
  background-size: contain;
  background-positon: center;
  background-repeat: no-repeat;

  @media (max-width: 414px) {
    height: 300px;
  }
`

const CatDetail = Styled.div`
  font-size: 2rem;

  h2 {
    color: ${props => props.theme.primaryColor};
    font-size: 4rem;
  }

  @media (max-width: 414px) {
    padding: 0 30px;
  }
`

const CatHeader = Styled.div`
  display: flex;
  align-items: center;

  img {
    width: 50px;
    height: auto;
  }

  span {
    margin-left: 30px;
  }
`

const CatDescription = Styled.p`
  padding: 20px 0;
  line-height: 1.6;
`

const CatAbility = Styled.div`
  width: 100%;
  height: auto;
  display: flex;
  justify-content: space-between;
  align-items: center;
  text-transform: uppercase;

  @media (max-width: 414px) {
    flex-direction: column;
    padding: 10px 0;

    label {
      padding-bottom: 5px;
    }
  }
`

const AbilityStar = Styled.img`
  background-image: url('${props => props.src}');
  margin-left: 10px;
  width: 32px;
  height: 32px;
`