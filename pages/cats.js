import React, { Component } from 'react'
import Link from 'next/link'
import Styled, { keyframes } from 'styled-components'
import fetch from 'isomorphic-unfetch'

import Nav from '../components/Nav'

class Cats extends Component {
  state = {
    loading: false,
    cat: null
  }

  async componentDidMount() {
    this.setState({ loading: true })
    const idArr = []
    const catArr = []
    const id = await fetch('https://api.thecatapi.com/v1/breeds')
    const idJson = await id.json()
    idJson.map(item => {
      idArr.push(item.id)
    })
    const cutItOff = idJson.splice(0, 15)
    for (let i = 1; i <= cutItOff.length; i++) {
      const rand = Math.floor(Math.random() * idArr.length + 1)
      const res = await fetch(`https://api.thecatapi.com/v1/images/search?breed_ids=${idArr[rand]}&api_key=27fc9bb7-44e1-44ce-91a9-b8f3b426bca4`)
      const json = await res.json()
      catArr.push(json[0])
    }
    this.setState({ cat: catArr, loading: false })
  }

  render() {
    const renderCats = () => {
      const { loading, cat } = this.state
      if (!loading && cat) {
        return cat.map((item, index) => {
          if (item === undefined || item === null) return
          return (
            <Link href={`/cat/?id=${item.breeds[0].id}`} as={`/cat/${item.breeds[0].id}`} key={index}>
              <a>
                <CatBox>
                  <CatImage pic={item.url} />
                  <CatDetail>{item.breeds[0].name}</CatDetail>
                </CatBox>
              </a>
            </Link>
          )
        })
      } else {
        return (
          <Loading src={'/static/images/loading.svg'} alt={'cat loading'} />
        )
      }
    }

    return (
      <div>
        <Nav user={this.props.user} />
        <Section>
          {renderCats()}
        </Section>
      </div>
    )
  }
}

export default Cats

const LoadingAnimation = keyframes`
  0% {
    transform: rotate(-20deg);
  }

  50% {
    transform: rotate(20deg);
  }

  100% {
    transform: rotate(-20deg);
  }
`

const Loading = Styled.img`
  width: 10%;
  height: auto;
  background-image: url("${props => props.src}");
  background-repeat: no-repeat;
  background-position: center;
  animation: ${LoadingAnimation} 2.5s ease-in-out 0s infinite;
  position: absolute;
  left: 47%;
  top: 50%;
  transform: translate(-50%, -50%);

  @media (max-width: 414px) {
    width: 30%;
    left: calc(100% - 65%);
  }

`

const Section = Styled.div`
  width: 1180px;
  height: auto;
  margin: 0 auto;
  padding: 30px 0;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;

  @media (max-width: 414px) {
    width: 100%;
    flex-direction: column;
    padding: 20px;

    a {
      width: 100%;
    }
  }
`

const CatBox = Styled.div`
  flex: 0 30%;
  transition: 0.4s;
  padding: 20px 0;

  &:hover {
    transform: translateY(-10px);
  }
`

const CatImage = Styled.div`
  height: 250px;
  width: 355px;
  background-position: center center;
  background-size: cover;
  background-image: url("${props => props.pic}");
  border-radius: 5px 5px 0 0;

  @media (max-width: 414px) {
    width: 100%;
  }
`

const CatDetail = Styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  height: 50px;
  background-color: ${props => props.theme.secondaryColor};
  color: #fff;
  font-size: 24px;
  padding: 15px;
  border-radius: 0 0 5px 5px;
`