import React, { Component } from 'react'
import Router, { withRouter } from 'next/router'

import {
  createEditorState
} from 'medium-draft'
import mediumDraftExporter from 'medium-draft/lib/exporter';

import { convertToRaw } from 'draft-js';
import mediumDraftImporter from 'medium-draft/lib/importer';

import Styled from 'styled-components'
import jsCookie from 'js-cookie'

import Auth from '../hoc/auth'
import Nav from '../components/Nav'
import Editor from '../components/Editor'

class EditPost extends Component {
  state = {
    title: '',
    thumbnail: '',
    img: '',
    imageError: '',
    editorState: createEditorState(),
    isDisabled: false,
    error: '',
    auth: null
  }

  async componentDidMount() {
    const { query: { id } } = this.props.router
    const token = jsCookie.get('token') || ''
    const response = await fetch(`/post/${id}`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })
    const post = await response.json()
    const html = post.post.content
    const editorState = createEditorState(convertToRaw(mediumDraftImporter(html)))
    this.setState({
      title: post.post.title,
      editorState,
      auth: post.auth
    })
  }

  onInputChnge = event => {
    this.setState({
      title: event.target.value
    })
  }

  onFileChange = event => {
    this.setState({ imageError: '' })
    if(event.target.files[0]) {
      const mime = event.target.files[0].type || ''
      if(mime.search('svg') < 0) {
        this.setState({thumbnail: event.target.files[0], img: URL.createObjectURL(event.target.files[0])})
      } else {
        this.setState({ imageError: 'Please use png jpg only!' })
      }
    } else {
      this.setState({
        thumbnail: ''
      })
    }
  }

  onChange = editorState => {
    this.setState({ editorState })
  }

  onSubmit = async () => {
    const { id } = this.props.user
    const { query: { id: postId } } = this.props.router
    this.setState({ error: '' })
    const { title, thumbnail, imageError, editorState } = this.state
    if(!imageError) {
      const content = mediumDraftExporter(editorState.getCurrentContent())
      const formData = new FormData()
      formData.append('title', title)
      formData.append('thumbnail', thumbnail)
      formData.append('content', content)
      formData.append('author', id)
      formData.append('postid', postId)

      this.setState({ isDisabled: true })

      const res = await fetch('/post/postPost', {
        method: 'POST',
        body: formData
      })
      const post = await res.json()
      if(post.success) {
        Router.push({
          pathname: '/post',
          query: { id: post.blog._id }
        }, `/post?id=${post.blog._id}`)
      }
    } else {
      this.setState({ error: 'Please fill content' })
    }
  }
  
  render() {
    const { isDisabled, error, title, thumbnail, auth } = this.state
    if(auth === null) return <div>Loading...</div>
    if(!auth) Router.push('/')
    return (
      <React.Fragment>
        <Nav user={this.props.user}/>
        <Section>
          <SectionHeader>
            <span>{error}</span><button onClick={this.onSubmit} disabled={isDisabled} >Submit</button>
          </SectionHeader>
          <input
            className="input-form" 
            onChange={this.onInputChnge} 
            type="text" 
            name="title" 
            placeholder="TITLE" 
            value={title}
            required />
          <input 
            onChange={this.onFileChange} 
            type="file" 
            name="thumbnail" 
            required />
          {this.state.imageError }
          <Editor {...this.state} onChange={this.onChange} handleChange={this.handleChange} onSubmit={this.onSubmit} />
        </Section>
      </React.Fragment>
    )
  }
}

export default Auth(withRouter(EditPost))

const Section = Styled.section`
  width: 1180px;
  height: 100vh;
  margin: 0 auto;
  font-size: 20px;
  padding-top: 30px;

  .input-form {
    border: none;
    width: 50%;
    height: 40px;
    font-size: 28px;
  }

  .md-RichEditor-root {
    padding: 0;
  }

  @media (max-width: 414px) {
    width: 100%;
    padding: 0 20px;

    input[type=text] {
      width: 100%;
      margin 0 0 20px 0;
    }
  }
`

const SectionHeader = Styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  width: 100%;
  height: 50px;
  padding: 40px 0;

  span {
    margin-right: 20px;
    color: red;
  }

  button {
    height: 40px;
    width: 120px;
    border: none;
    background-color: ${props => props.theme.secondaryColor};
    font-size: 20px;
    color: #fff;
  }
`