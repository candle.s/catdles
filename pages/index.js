import React, { Component } from 'react'
import { withAmp } from 'next/amp'
import dynamic from 'next/dynamic'
import Styled from 'styled-components'

import Nav from '../components/Nav'
import HeroSec from '../components/HeroSec'
import CatShowSec from '../components/CatShowSec'

// const CatShowSec = dynamic(() => import('../components/CatShowSec'))

class Home extends Component {
  render() {
    return (
      <Container>
        <Nav user={this.props.user} />
        <Wrapper>
          <HeroSec />
        </Wrapper>
        <CatSection>
          <Wrapper>
            <div className="catSec">
              <CatShowSec />
            </div>
          </Wrapper>
        </CatSection>
      </Container>
    )
  }
}

export default withAmp(Home, { hybrid: true })

const Container = Styled.div`
  width: 100%;
`

const Wrapper = Styled.div`
  width: 1180px;
  height: 100%;
  margin: 0 auto;

  @media (max-width: 414px) {
    width: 100%;
  }
`
const CatSection = Styled.div`
  height: 400px;
  background-color: #e3e3e3;

  .catSec {
    display: flex;
    justify-content: space-around;
    align-items: center;
    height: 100%;

    @media (max-width: 414px) {
      flex-direction: column;
      height: auto;

      a {
        width: 100%;
        padding: 20px 0;
      }
    }
  }

  @media (max-width: 414px) {
    height: auto;
    padding: 20px 40px;
  }
`