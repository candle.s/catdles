import React, { Component } from 'react'
import Router from 'next/router'

import {
  createEditorState
} from 'medium-draft'
import mediumDraftExporter from 'medium-draft/lib/exporter';

import Styled from 'styled-components'

import Auth from '../hoc/auth'
import Nav from '../components/Nav'
import Editor from '../components/Editor'

class NewPost extends Component {
  state = {
    title: '',
    thumbnail: '',
    img: '',
    imageError: '',
    editorState: createEditorState(),
    isDisabled: false,
    error: ''
  }

  onInputChnge = event => {
    this.setState({
      title: event.target.value
    })
  }

  onFileChange = event => {
    this.setState({ imageError: '' })
    if(event.target.files[0]) {
      const mime = event.target.files[0].type || ''
      if(mime.search('svg') < 0) {
        this.setState({thumbnail: event.target.files[0], img: URL.createObjectURL(event.target.files[0])})
      } else {
        this.setState({ imageError: 'Please use png jpg only!' })
      }
    } else {
      this.setState({
        thumbnail: ''
      })
    }
  }

  onChange = editorState => {
    this.setState({ editorState })
  }

  onSubmit = async () => {
    const { id } = this.props.user
    this.setState({ error: '' })
    const { title, thumbnail, imageError, editorState } = this.state
    if(thumbnail && !imageError) {
      const content = mediumDraftExporter(editorState.getCurrentContent())
      const formData = new FormData()
      formData.append('title', title)
      formData.append('thumbnail', thumbnail)
      formData.append('content', content)
      formData.append('author', id)

      this.setState({ isDisabled: true })

      const res = await fetch('/post/postPost', {
        method: 'POST',
        body: formData
      })
      const post = await res.json()
      if(post.success) {
        Router.push({
          pathname: '/post',
          query: { id: post.blog._id }
        }, `/post?id=${post.blog._id}`)
      }
    } else {
      this.setState({ error: 'Please fill content' })
    }
  }
  
  render() {
    const { isDisabled, error } = this.state
    return (
      <React.Fragment>
        <Nav user={this.props.user}/>
        <Section>
          <SectionHeader>
            <span>{error}</span><button onClick={this.onSubmit} disabled={isDisabled} >Submit</button>
          </SectionHeader>
          <input className="input-form" onChange={this.onInputChnge} type="text" name="title" placeholder="TITLE" required />
          <input onChange={this.onFileChange} type="file" name="thumbnail" required />
          {this.state.imageError }
          <Editor {...this.state} onChange={this.onChange} handleChange={this.handleChange} onSubmit={this.onSubmit} />
        </Section>
      </React.Fragment>
    )
  }
}

export default Auth(NewPost)

const Section = Styled.section`
  width: 1180px;
  height: 100vh;
  margin: 0 auto;
  font-size: 20px;
  padding-top: 30px;

  .input-form {
    border: none;
    width: 50%;
    height: 40px;
    font-size: 28px;
  }

  .md-RichEditor-root {
    padding: 0;
  }

  @media (max-width: 414px) {
    width: 100%;
    padding: 0 20px;

    input[type=text] {
      width: 100%;
      margin 0 0 20px 0;
    }
  }
`

const SectionHeader = Styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  width: 100%;
  height: 50px;
  padding: 40px 0;

  span {
    margin-right: 20px;
    color: red;
  }

  button {
    height: 40px;
    width: 120px;
    border: none;
    background-color: ${props => props.theme.secondaryColor};
    font-size: 20px;
    color: #fff;
  }
`