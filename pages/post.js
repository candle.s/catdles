import React, { Component } from 'react'
import Head from 'next/head'
import Router from 'next/router'
import Link from 'next/link'
import jsCookie from 'js-cookie'
import renderHTML from 'react-render-html'

import Styled from 'styled-components'

import fetch from 'isomorphic-unfetch'

import Nav from '../components/Nav'

class Post extends Component {
  static async getInitialProps({req, res, query: { id }}) {

    const token = process.browser ? jsCookie.get('token') || '' : req.cookies.token || ''
    const baseUrl = req ? `${req.protocol}://${req.get('Host')}` : ''
    const response = await fetch(`${baseUrl}/post/${id}`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })
    if(response.status === 404) {
      res.statusCode = 404
      res.end('not found')
      return
    }
    const post = await response.json()
    return { post }
  }

  deletePost = async (id) => {
    const token = jsCookie.get('token')

    if(confirm('are you sure ?')) {
      const res = await fetch(`/post/${id}`, {
        method: 'DELETE',
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({ id })
      })
      const response = await res.json()
      if(response.success) {
        Router.push('/')
      }
    }
  }

  render() {
    const { post, user } = this.props
    const renderPost = () => {
      const createdDate = post.post.createdAt.toString()
      const splitDate = new Date(createdDate).toLocaleDateString().split('/')
      const date = `${splitDate[1]}/${splitDate[0]}/${splitDate[2]}`
      return (
        <Article>
          <h1>{post.post.title}</h1>
          {renderHTML(post.post.content)}
          <PostAuthor>
            <img src={`/static/user/${post.post.author.img}`} alt={post.post.author.name} />
            <AuthorDetail>
              <span><strong>Author:</strong> {post.post.author.name}</span>
              <span><strong>Create:</strong> {date}</span>
            </AuthorDetail>
          </PostAuthor>
        </Article>
      )
    }

    return (
      <div>
        <Head>
          <title>{post.post.title}</title>
        </Head>
        <Nav user={user}/>
        <Section>
          {
            user && (
              post.auth && (
                <ActionPost>
                  <Link href={`/editpost?id=${post.post.id}`}>
                    <a><button>Edit</button></a>
                  </Link>
                  <button onClick={() => this.deletePost(post.post.id)}>Delete</button>
                </ActionPost>
              )
            )
          }
          {renderPost()}
        </Section>
      </div>
    )
  }
}

export default Post

const Section = Styled.div`
  width: 1180px;
  margin: 0 auto;
  height: auto;
  padding: 50px;
  font-size: 20px;

  @media (max-width: 414px) {
    width: 100%;
    padding: 20px 10px;
  } 
`

const ActionPost = Styled.div`
  display: flex;
  justify-content: flex-end;

  button {
    width: 120px;
    height: 30px;
    font-size: 18px;
    color: #fff;
    border: none;
    background-color: ${props => props.theme.secondaryColor};
    margin-left: 10px;
    cursor: pointer;
  }
`

const Article = Styled.div`
  h1 {
    padding: 20px 0;
  }
  
  ul {
    padding: 20px 30px;
  }

  blockquote {
    border-left: 5px solid #4ca8de;
    color: #555;
    font-size: 1.2em;
    margin: 0;
    padding: 10px 0 10px 20px;
    background-color: #e2f2ff;
  }

  @media (max-width: 414px) {
    blockquote {
      font-size: .6em;
    }
  }
`

const PostAuthor = Styled.div`
  margin: 20px 0;
  display: flex;
  align-items: center;
  height: 100px;
  width: 400px;
  background-color: #eee;
  padding: 0 20px;
  border-radius: 4px;

  img {
    width: 50px;
    height: 50px;
    border-radius: 50%;
  }

  span {
    padding: 0 50px;
  }

  @media (max-width: 414px) {
    width: 100%;
  }
`

const AuthorDetail = Styled.div`
  display: flex;
  flex-direction: column;

  span:last-child {
    padding-top: 5px;
  }

  @media (max-width: 414px) {
    font-size: 1.6rem;
  }
`