import React, { Component } from 'react'
import Styled from 'styled-components'
import { Formik, Form, Field, ErrorMessage } from 'formik';

import Nav from '../components/Nav'
import withAuth from '../hoc/auth'

class Profile extends Component {
  state = {
    isLoading: false,
    isUpload: false,
    img: this.props.user.img ? `/static/user/${this.props.user.img}` : "/static/images/user.svg",
    imageError: '',
    imageUpload: '',
    options: [
      {value: '', name: 'Please select'},
      {value: 'male', name: 'Male'},
      {value: 'female', name: 'Female'},
      {value: 'etc', name: 'etc'},
    ],
    initialValues: { 
      email: this.props.user.email, 
      name: this.props.user.name, 
      gender: this.props.user.gender, 
      age: this.props.user.age 
    }
  }

  uploadImage = async () => {
    const { imageUpload } = this.state
    if(imageUpload !== '') {
      this.setState({ isUpload: true })
      const formData = new FormData()
      formData.append('img', this.state.imageUpload)
      formData.append('id', this.props.user.id)
      const res = await fetch('/user/upload', {
        method: 'POST',
        body: formData
      })
      const response = await res.json()
      if(response.success) {
        this.setState({
          img: `/static/user/${response.upload}`,
          isUpload: false
        })
      }
    }
  }

  changeImage = async (e) => {
    this.setState({ imageError: '' })
    if(e.target.files[0]) {
      const mime = e.target.files[0].type || ''
      if(mime.search('svg') < 0) {
        this.setState({imageUpload: e.target.files[0], img: URL.createObjectURL(e.target.files[0])})
      } else {
        this.setState({ imageError: 'Please use png jpg only!' })
      }
    }
  }

  submitForm = async (values) => {
    this.setState({ isLoading: true })
    const res = await fetch('/user/change_profile', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({
        ...values,
        id: this.props.user.id
      })
    })
    const response = await res.json()
    this.setState({ isLoading: false })
    if(response.success) {
      window.location.href = '/'
    }
  }

  render() {
    const { user } = this.props
    return (
      <div>
        <Nav user={this.props.user} />
        <Section>
          <h1>Profile</h1>
          <SectionProfile>
            <ProfileImgage>
              <label htmlFor="profileImage">
                <img id="img-profile" src={this.state.img} />
                <input onChange={this.changeImage} type="file" name="profileImage" id="profileImage" style={{display: 'none'}} />
              </label>
              <button onClick={this.uploadImage}>UPLOAD</button>
              <p>{this.state.imageError}</p>
              {this.state.isUpload &&
                <p>Loading...</p>
              }
            </ProfileImgage>
            <Detail>
              <Formik
                initialValues={this.state.initialValues}
                validate={values => {
                  let errors = {};
                  if (!values.email) {
                    errors.email = 'Required';
                  } else if (
                    !/^-?\d*\.?\d*$/.test(values.age) 
                  ) {
                    errors.age = 'number only'
                  } else if(values.age.length > 3) {
                    errors.age = 'your age > 999 ?'
                  }
                  return errors;
                }}
                onSubmit={(values, { setSubmitting }) => {
                  setSubmitting(false);
                  this.submitForm(values)
                }}
              >
                {({ isSubmitting }) => (
                  <Form>
                    <InputGroup>
                      <label htmlFor="email">email: </label>
                      <div className="group">
                        <Field type="email" name="email" />
                        <ErrorMessage name="email" component="span" className="error-message" />
                      </div>
                    </InputGroup>
                    <InputGroup>
                      <label htmlFor="name">name: </label>
                      <div className="group">
                        <Field type="text" name="name" />
                        <ErrorMessage name="name" component="span" className="error-message" />
                      </div>
                    </InputGroup>
                    <InputGroup>
                      <label htmlFor="gender">gender: </label>
                      <div className="group">
                        <Field component="select" name="gender">
                          {
                            this.state.options.map(item => {
                              if(item.value === this.props.user.gender) {
                                return <option value={item.value} key={item.name}>{item.name}</option>
                              }
                              return <option value={item.value} key={item.name}>{item.name}</option>
                            })
                          }
                        </Field>
                      </div>
                      <ErrorMessage name="gender" component="span" className="error-message" />
                    </InputGroup>
                    <InputGroup>
                      <label htmlFor="age">age: </label>
                      <div className="group">
                        <Field type="text" name="age" />
                        <ErrorMessage name="age" component="span" className="error-message" />
                      </div>
                    </InputGroup>
                    <button type="submit" disabled={isSubmitting}>
                      Submit
                    </button>
                    {this.state.isLoading &&
                      <p>Loading...</p>
                    }
                  </Form>
                )}
              </Formik>
            </Detail>
          </SectionProfile>
        </Section>
      </div>
    )
  }
}

export default withAuth(Profile)

const Section = Styled.section`
  width: 1180px;
  height: 100vh;
  margin: 0 auto;
  padding: 50px 0;

  h1 {
    text-transform: uppercase;
    color: ${props => props.theme.primaryColor};
    font-size: 36px;
    text-align: center;
  }

  @media (max-width: 414px) {
    width: 100%;
    padding: 20px;
  }
`
const SectionProfile = Styled.div`
  display: flex;
  justify-content: space-between;
  padding: 50px 0;

  @media (max-width: 414px) {
    flex-wrap: wrap;
  }
`

const ProfileImgage = Styled.div`
  flex: 0 50%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  
  img {
    width: 200px;
    height: auto;
  }

  button {
    margin: 20px 0;
    width: 200px;
    height: 40px;
    border: none;
    background: ${props => props.theme.primaryColor};
    border-radius: 5px;
    color: #fff;
    font-size: 26px;
  }

  @media (max-width: 414px) {
    flex: 1;
  }
`

const Detail = Styled.div`
  flex: 1;

  form {
    padding: 40px;

    button {
      width: 30%;
      height: 40px;
      border: none;
      background: ${props => props.theme.primaryColor};
      color: #fff;
      font-size: 26px;
      text-transform: uppercase;
      border-radius: 5px;
    }

    &:last-child {
      text-align: center;
    }

    @media (max-width: 414px) {
      button {
        width: 100%;
      }
    }
  }
`

const InputGroup = Styled.div`
  display: flex;
  align-items: center;
  font-size: 26px;
  padding: 20px 0;
  flex-wrap: wrap;

  .group {
    flex: 1;
    text-align: left;
  }

  .error-message {
    color: red;
  }

  label {
    text-transform: uppercase;
    flex: 0 30%;
    color: ${props => props.theme.secondaryColor};
  }
  input, select {
    width: 100%;
    height: 40px;
    border: none;
    border-radius: 5px;
    background: #ccc;
    color: #fff;
    padding: 0 10px;
    font-size: inherit;
  }

  @media (max-width: 414px) {
    label {
      font-size: 16px;
    }
  }
`