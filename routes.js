const routes = require('next-routes')

module.exports = routes()
.add('post', '/post/:id')
.add('editpost', '/editpost/:id')