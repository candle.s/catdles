const fs = require('fs')
const jwt = require('jsonwebtoken')
const blogSchema = require('../models/post')
const { validationResult } = require('express-validator/check')

const { resizeImage } = require('../lib/utils')

exports.getPosts = async (req, res, next) => {
  const posts = await blogSchema.find({}).sort({ createdAt: 'desc' })
  if(!posts) return res.status(404).send('not found.')
  return res.status(200).json(posts)
}

exports.postPost = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const resultErrors = errors.array().map(error => {
      return error.msg;
    });
    return res.status(422).json({
      errors: errors.array().length,
      msg: resultErrors,
      result: []
    });
  }

  const title = req.body.title
  const subtitle = req.body.subtitle || ''
  const content = req.body.content
  let thumbnail
  const author = req.body.author
  const postId = req.body.postid

  if(postId) {
    const post = await blogSchema.findById(postId)
    if(req.files.length < 1) {
      thumbnail = post.thumbnail
      const blog = await blogSchema.findOneAndUpdate({_id: postId}, { title, subtitle, content, thumbnail, author })
      return res.status(200).json({blog, success: true})
    } else {
      fs.unlink(`./static/post/${post.thumbnail}`, (err) => {
        if(err) return console.log(err)
      })
      resizeImage(req.files[0].filename)
      thumbnail = `optimize-${req.files[0].filename}`
      const blog = await blogSchema.findOneAndUpdate({_id: postId}, { title, subtitle, content, thumbnail, author })
      return res.status(200).json({blog, success: true})
    }
  } else {
    resizeImage(req.files[0].filename)
    thumbnail = `optimize-${req.files[0].filename}`
    const Blog = new blogSchema({
      title,
      subtitle,
      content,
      thumbnail,
      author
    })
    const blog = await Blog.save()
    return res.status(201).json({blog, success: true})
  }
}

exports.getPost = async (req, res, next) => {
  const id = req.params.id
  const token = req.headers.authorization.split(' ')[1]
  const post = await blogSchema.findById(id).populate('author', 'name img')
  if(!post) return res.status(404).send('not found.')
  if(token) {
    jwt.verify(token, process.env.SECRET_KEY, null, async (err, decoded) => {
      if(err) return res.status(500).send('something wrong.')
      if(decoded.id.toString() === post.author._id.toString()) {
        return res.status(200).json({ 
          post: {
            id: post._id,
            title: post.title,
            thumbnail: post.thumbnail,
            createdAt: post.createdAt,
            content: post.content,
            author: {
              img: post.author.img,
              name: post.author.name
            }
          },
          auth: true 
        })
      } else {
        return res.status(200).json({ 
          post: {
            id: post._id,
            title: post.title,
            thumbnail: post.thumbnail,
            createdAt: post.createdAt,
            content: post.content,
            author: {
              img: post.author.img,
              name: post.author.name
            }
          }, 
          auth: false 
        })
      }
    })
  } else {
    return res.status(200).json({
      post: {
        id: post._id,
        title: post.title,
        thumbnail: post.thumbnail,
        createdAt: post.createdAt,
        content: post.content,
        author: {
          img: post.author.img,
          name: post.author.name
        }
      }
    })
  }
}

exports.uploadImagePost = (req, res, next) => {
  const image = req.file.filename
  if(image) {
    return res.status(200).json({url: `/static/post/${image}`, success: true})
  }
}

exports.deletePost = async (req, res, next) => {
  const postId = req.body.id
  const userId = req.user._id
  const post = await blogSchema.findOne({ _id: postId })
  if(post) {
    if(post.author.toString() === userId.toString()) {
      const result = await blogSchema.findByIdAndDelete(postId)
      if(result) {
        fs.unlink(`./static/post/${result.thumbnail}`, err => {
          if(err) return console.log(err)
        })
        return res.status(200).json({
          success: true,
          result
        })
      } else {
        return res.status(404).end()
      }
    } else {
      return res.status(400).json({
        success: 'false',
        msg: 'You don\'t have permission.'
      })
    }
  }
}