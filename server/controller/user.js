const fs = require('fs')
const { validationResult } = require('express-validator/check')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')

const userSchema = require('../models/user')
const { resizeImage, resizeImageUser } = require('../lib/utils')

exports.userRegister = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const resultErrors = errors.array().map(error => {
      return error.msg;
    });
    return res.status(422).json({
      errors: errors.array().length,
      msg: resultErrors,
      result: []
    });
  }

  const email = req.body.email
  const password = req.body.password

  try {
    const salt = await bcrypt.genSalt(10)
    const hashPassword = await bcrypt.hash(password, salt)

    const user = await userSchema.findOne({ email: email })

    if (user) {
      return res.status(200).json({
        success: false,
        msg: 'User is already!'
      })
    }

    const User = new userSchema({
      email,
      password: hashPassword
    })
    const result = await User.save()

    const token = await jwt.sign({ id: result.id }, process.env.SECRET_KEY, { expiresIn: '1d' })

    res.cookie('token', token, { maxAge: '86400000'})

    req.user = result

    return res.status(201).json({
      success: true,
      email: result.email,
      img: '',
      token
    })

  } catch (e) {
    console.error(e);
  }
}

exports.userLogin = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const resultErrors = errors.array().map(error => {
      return error.msg;
    });
    return res.status(422).json({
      errors: errors.array().length,
      msg: resultErrors,
      result: []
    });
  }

  const email = req.body.email
  const password = req.body.password

  const user = await userSchema.findOne({ email })
  if (!user) return res.status(422).json({
    success: false,
    msg: 'User not found'
  })
  const isMatch = await bcrypt.compare(password, user.password)
  if (!isMatch) return res.status(422).json({
    success: false,
    msg: 'Password incorrect!'
  })

  const token = await jwt.sign({ id: user.id }, process.env.SECRET_KEY, { expiresIn: '1d' })

  res.cookie('token', token, { maxAge: '86400000'})

  req.user = user

  return res.status(200).json({
    success: true,
    email: user.email,
    img: user.img,
    token
  })
}

exports.uploadImage = async (req, res, next) => {
  const file = req.file.filename
  const id = req.body.id
  const result = await userSchema.findOne({_id: id}).select('img -_id')
  const image = result.img
  if(image !== '') {
    fs.unlink(`./static/user/${image}`, (err) => {
      if(err) return res.status(200).json({ success: false, msg: err })
    })
  }
  if(!file) return res.status(200).json({ success: false, msg: 'Please upload png jpg only!' })
  resizeImageUser(file)
  const user = await userSchema.findOneAndUpdate({ _id: id }, { img: `optimize-${file}` })
  if(user) {
    return res.json({
      success: true,
      upload: `optimize-${file}`
    })
  }
} 

exports.getUser = async (req, res, next) => {
  const id = req.body.id
  const user = await userSchema.findOne({_id: id})
  if(!user) {
    return res.status(404).send('Not found')
  }
  return res.status(200).json({
    id: user._id,
    name: user.name,
    email: user.email,
    img: user.img,
    gender: user.gender,
    age: user.age
  })
}

exports.chageProfile = async (req, res, next) => {
  const id = req.body.id
  const name = req.body.name
  const gender = req.body.gender
  const age = parseInt(req.body.age) || 0

  try {
    const user = await userSchema.findOneAndUpdate({ _id: id }, { name, gender, age }, {new: true})
    if(!user) { return res.status(422).json({ success: false, msg: 'Something wrong' }) }
    return res.status(200).json({
      success: true,
      name: user.name,
      gender: user.gender,
      age: user.age
    })
  } catch (err) {
    console.log(err) 
  }
}