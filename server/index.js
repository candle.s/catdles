const express = require('express')
const routes = require('../routes')
const cookieParser = require('cookie-parser')
const compression = require('compression')
const cors = require('cors')
const helmet = require('helmet')
const nodemailer = require('nodemailer')
const next = require('next')
const mongoose = require('mongoose')
const userSchema = require('./models/user')
const subscribeSchema = require('./models/subscribe')
require('dotenv').config()

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = routes.getRequestHandler(app)

const jwt = require('jsonwebtoken')
const bodyParser = require('body-parser')

const userRoutes = require('./routes/user')
const postRoutes = require('./routes/post')

mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true }, (err) => {
  if (err) console.log(err);
  console.log('Goooo!');
})

app.prepare().then(() => {
  const server = express()

  server.use(cors())
  server.use(compression())
  server.use(helmet())
  server.use(cookieParser(process.env.COOKIE_KEY))
  server.use(bodyParser.json())
  server.use(bodyParser.urlencoded({ extended: false }))

  server.use('/post', postRoutes)
  server.use('/user', userRoutes)

  server.post('/authentication', async (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1]
    if (token) {
      jwt.verify(token, process.env.SECRET_KEY, null, async (err, decoded) => {
        if(err) {
          return res.status(200).json(null)
        } else {
          const user = await userSchema.findById(decoded.id)
          if (user) {
            return res.status(200).json({
              id: user.id,
              email: user.email,
              img: user.img,
              name: user.name,
              gender: user.gender,
              age: user.age
            })
          }
        }
      })
    } else {
      return res.status(200).json(null)
    }
  })

  server.post('/sendmail', async (req, res) => {
    const email = req.body.email
    const subscribe = await subscribeSchema.findOne({ email })
    if(!email) return res.status(400).json({
      success: false,
      msg: 'please fill email'
    })
    if(subscribe) return res.status(400).json({
      success: false,
      msg: 'you have already subscribe'
    })
    const newSubScribe = new subscribeSchema({
      email
    })
    const result = await newSubScribe.save()
    if(result) {
      const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: process.env.SENDER_MAIL,
          pass: process.env.SENDER_PASS
        }
      })
  
      let mailOptions = {
        from: process.env.SENDER_MAIL,
        to: email,               
        subject: 'Catdles mailer',             
        html: `<b>Thank you for subscribe</b><br><img src="/static/pet.png" alt="catdles" />`,
      }
  
      transporter.sendMail(mailOptions, function (err, info) {
        if(err) return res.status(400).json({ success: false, msg: 'something wrong.' })
        return res.status(200).json({ success: true, msg: 'thank you.' })
     })
    }
  })

  server.get('/cat/:id', (req, res) => {
    return app.render(req, res, '/cat', { id: req.params.id })
  })
  
  server.get('*', (req, res) => {
    return handle(req, res)
  })

  server.use(handle).listen(process.env.PORT || 3000, err => {
    if (err) throw err
    console.log(`🚀`);
  })
}).catch((ex) => {
  console.error(ex.stack);
  process.exit(1);
})
