const userSchema = require('../models/user')

async function authentication(id) {
  const user = await userSchema.findById(id)
  return user
}

module.exports = authentication