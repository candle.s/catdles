const fs = require('fs')
const sharp = require('sharp')

exports.resizeImage = async (filename) => {
  try {
    const resize = await sharp(`./static/post/${filename}`)
    .resize(700)
    .withMetadata()
    .jpeg({ quality: 90 })
    .png({ quality: 90 })
    .webp({ quality: 90 })
    .toFile(`./static/post/optimize-${filename}`)
    fs.unlink(`./static/post/${filename}`, (err, result) => {
      if(err) return console.log(err)
      return resize
    })
  } catch (err) {
    console.log(err)
  }
}

exports.resizeImageUser = async (filename) => {
  try {
    const resize = await sharp(`./static/user/${filename}`)
    .resize(200)
    .withMetadata()
    .jpeg({ quality: 90 })
    .png({ quality: 90 })
    .webp({ quality: 90 })
    .toFile(`./static/user/optimize-${filename}`)
    fs.unlink(`./static/user/${filename}`, (err, result) => {
      if(err) return console.log(err)
      return resize
    })
  } catch (err) {
    console.log(err)
  }
}