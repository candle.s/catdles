const mongoose = require('mongoose')
const blogSchema = mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  subTitle: {
    type: String
  },
  content: {
    type: String
  },
  thumbnail: {
    type: String,
    required: true
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  }
}, {timestamps: true})

const Post = mongoose.model('post', blogSchema)
module.exports = Post