const mongoose = require('mongoose')
const subscribeSchema = mongoose.Schema({
  email: {
    type: String,
    required: true
  }
}, { timestamp: true })

const Subscribe = mongoose.model('subscribe', subscribeSchema)
module.exports = Subscribe