const mongoose = require('mongoose')
const userSchema = mongoose.Schema({
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  name: {
    type: String,
    default: ''
  },
  gender: {
    type: String,
    default: ''
  },
  age: {
    type: Number,
  },
  img: {
    type: String,
    default: ''
  }
}, { timestamps: { createdAt: 'created_at' } })

const User = mongoose.model('user', userSchema)
module.exports = User