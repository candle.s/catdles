const express = require('express')
const router = express.Router()
const multer = require('multer')
const jwt = require('jsonwebtoken')

const userSchema = require('../models/user')

const { body, check } = require('express-validator/check')

const { 
  getPosts, 
  getPost, 
  postPost,
  uploadImagePost,
  deletePost
} = require('../controller/post')

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './static/post')
  },
  filename: function (req, file, cb) {
    const mimeType = file.mimetype.split('/')
    const extension = mimeType[mimeType.length - 1]
    if(extension === 'png' || extension === 'jpeg' || extension === 'jpg') {
      cb(null, `${file.fieldname}-${Date.now()}.${extension}`)
    } else {
      return false
    }
  }
})

const upload = multer({ storage: storage })

const storageImage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './static/post')
  },
  filename: function (req, file, cb) {
    const mimeType = file.mimetype.split('/')
    const extension = mimeType[mimeType.length - 1]
    if(extension === 'png' || extension === 'jpeg' || extension === 'jpg') {
      cb(null, `${file.fieldname}-${Date.now()}.${extension}`)
    } else {
      return false
    }
  }
})

const authenUser = (req, res, next) => {
  const token = req.headers.authorization.split(' ')[1]
  if(token) {
    jwt.verify(token, process.env.SECRET_KEY, null, async (err, decoded) => {
      if(err) return res.status(500).json({ success: false, msg: 'Something wrong.' })
      const user = await userSchema.findById(decoded.id)
      if(!user) return res.status(500).json({ success: false, msg: 'you are not permission.' })
      req.user = user
      console.log(req.user)
      next()
    })
  } else {

  }
} 

const uploadImage = multer({ storage: storageImage })

router.post('/image_post', uploadImage.single('image'), uploadImagePost)

router.get('/posts', getPosts)

router.get('/:id', getPost)

router.post('/postPost', upload.any('thumbnail'), postPost)

router.delete('/:id', authenUser, deletePost)

module.exports = router