const express = require('express')
const router = express.Router()
const multer = require('multer')

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './static/user')
  },
  filename: function (req, file, cb) {
    const mimeType = file.mimetype.split('/')
    const extension = mimeType[mimeType.length - 1]
    if(extension === 'png' || extension === 'jpeg' || extension === 'jpg') {
      cb(null, `${file.fieldname}-${Date.now()}.${extension}`)
    } else {
      return false
    }
  }
})

const upload = multer({ storage: storage })

const { body } = require('express-validator/check')

const {
  userRegister,
  userLogin,
  uploadImage,
  getUser,
  chageProfile
} = require('../controller/user')

router.post('/register', [
  body('email')
    .isEmail()
    .trim()
    .not()
    .isEmpty()
    .withMessage('email is required'),
  body('password')
    .isLength({ min: '6' })
    .trim()
    .not()
    .isEmpty()
    .withMessage('password is required')
], userRegister)

router.post('/login', [
  body('email')
    .isEmail()
    .trim()
    .not()
    .isEmpty()
    .withMessage('email is required'),
  body('password')
    .isLength({ min: '6' })
    .trim()
    .not()
    .isEmpty()
    .withMessage('password is required')
], userLogin)

router.post('/upload', upload.single('img'), uploadImage)

router.get('/getUser', getUser)

router.post('/change_profile', chageProfile)

module.exports = router